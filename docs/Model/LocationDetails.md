# # LocationDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**\MyBit\Ebay\Inventory\Model\Address**](Address.md) |  | [optional]
**geo_coordinates** | [**\MyBit\Ebay\Inventory\Model\GeoCoordinates**](GeoCoordinates.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
