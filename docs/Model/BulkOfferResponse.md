# # BulkOfferResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**responses** | [**\MyBit\Ebay\Inventory\Model\OfferSkuResponse[]**](OfferSkuResponse.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
