#!/bin/bash

openapi-generator generate -i sell_inventory_v1_oas3.yaml -g php -o . --additional-properties=apiPackage=Api,invokerPackage=MyBit\\Ebay\\Inventory,artifactVersion=1.1.0,composerPackageName=my-bit/ebay-inventory-api